# Experiments in lyrical modeling

Final project for Spring 2019 Corpus Linguistics class.

`src` contains the source code used.

`writeup` contains the final paper (pdf and tex format, with bibliography file) and presentation handout.

# `src`

`scraper_timeout.py` does the Bandcamp scraping.

`vectorization.py` trains the Word2Vec models used for the rest of the project code.

`nn.py` trains the neural networks to do genre prediction.

`outliers.py` finds artists whose similarity to their neighbors in tag space and in lyric space are highly different, and identifies outliers based on these measures.

`interactive.py` starts an interacti session and lets the user look up words/phrases in either Word2Vec model, and to predict the genre of arbitrary input text.