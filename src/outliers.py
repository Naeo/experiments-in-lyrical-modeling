"""
Identify artists whose lyrics neighbors are highly unlike their tag neighbors.
"""
from collections import defaultdict
from functools import partial
import json
from multiprocessing import Pool
import os

from gensim.models.word2vec import Word2Vec
from gensim.models.keyedvectors import KeyedVectors
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import strip_punctuation, strip_multiple_whitespaces
from hdbscan import HDBSCAN
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from nn import tqdm

def normalize(column):
    """Z-score transformation on a DataFrame column."""
    try:
        return (column - column.mean()) / column.std()
    except:
        return column

def transform_data_debug(text, w2v):
    """
    Given some text (list of list of str) and a path to a word2vec model,
    transform the text using the word2vec model.
    """
    text = np.array([
            w2v.wv[j]
            if j in w2v.wv
            else np.zeros(w2v.wv.vectors.shape[1])
            for j in text
    ])
    text = np.sum(text, axis=0, keepdims=False)
    if np.any(text):
        text = text / np.linalg.norm(text)

    return text

def _load_line(s):
    """
    Multithreadable function to process a single line from the data file.
    Returns an empty dict if there are any errors.
    """
    try:    j = json.loads(s.lower())
    except: return {}
    if not isinstance(j, dict): return {}
    curtrack = {
        "artist": j["artist"],
        "lyrics": simple_preprocess(j["lyrics"]),
        "tags": [strip_multiple_whitespaces(strip_punctuation(i)).strip() for i in j["tags"]],
    }

    curtrack["tags"] = [W for T in curtrack["tags"] for W in simple_preprocess(T)]

    if len(curtrack["lyrics"]) >= 20:
        return curtrack
    else:
        return {}

def load_data_mp(n_threads=3, debug_nrows=None):
    if os.path.isfile("models/lyrics keyed vectors") and os.path.isfile("models/tag keyed vectors"):
        lyrics = KeyedVectors.load("models/lyrics keyed vectors")
        tags   = KeyedVectors.load("models/tag keyed vectors")
        return tags, lyrics

    tracks = []

    with Pool(n_threads) as P:
        res = P.imap_unordered(
            _load_line,
            tqdm(open("Track Data.json", "r", encoding="utf8"), desc="Loading data"),
            chunksize=100_000,
        )
        for i in res:
            if i: tracks.append(i)
            if debug_nrows is not None and len(tracks) >= debug_nrows: break

    tags = defaultdict(list)
    lyrics = defaultdict(list)
    for i in tqdm(tracks, desc="Collecting tags and text"):
        tags[i["artist"]] += i["tags"]
        lyrics[i["artist"]] += i["lyrics"]

    valid_ks = []
    for K in tags.keys():
        if len(lyrics[K]) > 200 and len(tags[K]) >= 5:
            valid_ks.append(K)

    if not os.path.isfile("models/lyrics keyed vectors"):
        lyrics_w2v = Word2Vec.load("models/word2vec/lyrics")
        lyrics_vecs = KeyedVectors(300)
        with Pool(n_threads) as P:
            vecs = P.map(
                partial(transform_data_debug, w2v=lyrics_w2v),
                tqdm([lyrics[k] for k in valid_ks], desc="Transforming lyrics into vectors"),
            )
            vecs = np.array(vecs)
        # vecs = np.array([
        #     transform_data_debug(lyrics[K], lyrics_w2v)
        #     for K in tqdm(valid_ks, desc="Transforming lyrics to vectors", position=0)
        # ])
        lyrics_vecs.add(valid_ks, vecs)
        lyrics_vecs.save("models/lyrics keyed vectors")
    else:
        lyrics = KeyedVectors.load("models/lyrics keyes vectors")

    if not os.path.isfile("models/tag keyed vectors"):
        tag_w2v = Word2Vec.load("models/word2vec/tags")
        tag_vecs = KeyedVectors(300)
        with Pool(n_threads) as P:
            vecs = P.map(
                partial(transform_data_debug, w2v=tag_w2v),
                tqdm([tags[k] for k in valid_ks], desc="Transforming tags into vectors"),
            )
            vecs = np.array(vecs)
        # vecs = np.array([
        #     transform_data_debug(tags[K], tag_w2v)
        #     for K in tqdm(valid_ks, desc="Transforming tags to vectors", position=0)
        # ])
        tag_vecs.add(valid_ks, vecs)
        tag_vecs.save("models/tag keyed vectors")
    else:
        tags = KeyedVectors.load("models/tag keyed vectors")

    return tags, lyrics

def get_distances(l_keyed_vectors, t_keyed_vectors, n=10):
    outfile = f"models/Artist Distances, n={n}.csv"

    if os.path.isfile(outfile):
        df = pd.read_csv(outfile)
        return df

    # Pulling this out into its own list makes debugging easier--
    # just index the first, say, thousand values in the list
    # to cut down on runtime of this function call.
    artists = sorted(list(l_keyed_vectors.vocab.keys()))

    # Calculate lyric --> tag distances
    lyric_neighbors = {
        k: [i[0] for i in l_keyed_vectors.most_similar(k, topn=n)]
        for k in tqdm(artists, desc="Getting most similar artists by lyrics")
    }
    lyric_lyric_dists = {
        k: np.mean(l_keyed_vectors.distances(k, lyric_neighbors[k]))
        for k in tqdm(lyric_neighbors, desc="Calculating Lyric-Lyric neighbor distances")
    }
    lyric_tag_dists = {
        k: np.mean(t_keyed_vectors.distances(k, lyric_neighbors[k]))
        for k in tqdm(lyric_neighbors, desc="Calculating Lyric-Tag neighbor distances")
    }

    # Now by tag neighbors
    tag_neighbors = {
        k: [i[0] for i in t_keyed_vectors.most_similar(k, topn=n)]
        for k in tqdm(artists, desc="Getting most similar artists by tags")
    }
    tag_lyric_dists = {
        k: np.mean(t_keyed_vectors.distances(k, tag_neighbors[k]))
        for k in tqdm(tag_neighbors, desc="Calculating Lyric-Lyric neighbor distances")
    }
    tag_tag_dists = {
        k: np.mean(l_keyed_vectors.distances(k, tag_neighbors[k]))
        for k in tqdm(tag_neighbors, desc="Calculating Lyric-Tag neighbor distances")
    }


    df = pd.DataFrame([
        {
            "Artist": k,
            "Lyric-Lyric distance": lyric_lyric_dists[k],
            "Lyric-Tag distance":   lyric_tag_dists[k],
            "Tag-Lyric distance":   tag_lyric_dists[k],
            "Tag-Tag distance":     tag_tag_dists[k],
        }
        for k in artists
    ])

    # Normalize the distances, since we have distances in two completely different spaces.
    for COL in df:
        if COL == "Artist": pass
        df[COL] = normalize(df[COL])

    # Add the columns for the difference in average distances.
    df["Difference: Lyric-Lyric minus Lyric-Tag"] = df["Lyric-Lyric distance"] - df["Lyric-Tag distance"]
    df["Difference: Tag-Tag minus Tag-Lyric"] = df["Tag-Tag distance"] - df["Tag-Lyric distance"]


    # use HDBSCAN to identify outlier points.
    df = df.dropna(axis=0, how="any")
    df["Is outlier"] = HDBSCAN(
        min_cluster_size=50,
        min_samples=15,
    ).fit_predict(
            df[[
                "Difference: Lyric-Lyric minus Lyric-Tag",
                "Difference: Tag-Tag minus Tag-Lyric",
            ]].values
    ) == -1

    # Add the euclidean distance from the origin
    df["Mahalanobis Distance from Origin"] = np.linalg.norm(
        df[[
            "Difference: Lyric-Lyric minus Lyric-Tag",
            "Difference: Tag-Tag minus Tag-Lyric",
        ]].values,
        axis=1,
    )

    # Mahalanobis distances in 2d are chi-distributed, assuming a normal distribution
    # of the data.  So we can add the p-value according to the equation:
    # p = 1 - exp(-t^2 / 2)
    # for some distance t.
    df["Mahalanobis Probability"] = 1 - np.exp((-df["Mahalanobis Distance from Origin"].values ** 2) / 2)

    df.to_csv(outfile, index=False)

    return df

if __name__ == "__main__":
    tag_wv, lyric_wv = load_data_mp(3)
    dists = get_distances(lyric_wv, tag_wv, 100)

    plt.figure(figsize=(5,5))
    plt.scatter(
        dists["Difference: Lyric-Lyric minus Lyric-Tag"],
        dists["Difference: Tag-Tag minus Tag-Lyric"],
        c=dists["Is outlier"],
        s=1,
        alpha=0.5,
        cmap="seismic",
    )
    plt.title("Outliers identified by HDBSCAN")
    plt.xlabel("Difference: Lyric-Lyric minus Lyric-Tag")
    plt.ylabel("Difference: Tag-Tag minus Tag-Lyric")
    plt.savefig("Artist Distances (Outliers).png", dpi=600)

    plt.figure(figsize=(5,5))
    plt.scatter(
        dists["Difference: Lyric-Lyric minus Lyric-Tag"],
        dists["Difference: Tag-Tag minus Tag-Lyric"],
        c=dists["Mahalanobis Distance from Origin"],
        s=1,
        alpha=0.5,
        cmap="jet",
    )
    plt.title("Mahalanobis Distances from origin")
    plt.xlabel("Difference: Lyric-Lyric minus Lyric-Tag")
    plt.ylabel("Difference: Tag-Tag minus Tag-Lyric")
    plt.savefig("Artist Distances (Mahalanobis).png", dpi=600)
    plt.show()
