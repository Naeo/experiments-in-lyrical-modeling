import json
import multiprocessing
import os
import random
import re
from time import sleep
from urllib.request import urlopen

import bs4
from func_timeout import func_timeout
from func_timeout.exceptions import FunctionTimedOut
import requests
import tqdm


"""
Utility functions
"""
def _write_out(iterable, outfile):
    """
    Write out each string in `iterable` to the file `outfile`,
    opened in append mode.
    """
    with open(outfile, "a", encoding="utf8") as F:
        for i in iterable:
            F.write(i.strip())
            F.write("\n")

    return 0

def _dump_track_data(data, trackfile="Track Data.json"):
    """
    Given an iterable of dicts, dump them out to a JSON file.
    """
    with open(trackfile, "a", encoding="utf8") as F:
        for TRACK in data:
            F.write(json.dumps(TRACK).strip())
            F.write("\n")

    return 0

"""
Scraping functions
"""
def get_artist_pages(base_url="https://bandcamp.com/artist_index"):
    """
    For a given page in Bandcamp's Artist Index, grab links
    to all the artists listed on that page.

    :param base_url: str
        The URL to scrape artist links from.
    """
    try:
        data = urlopen(base_url)
    except Exception as e:
        print(e)
        sleep(60)
        return get_artist_pages(base_url)

    # If we 404, that probably means we've hit the end of
    # the pages that exist and we need to stop.
    if data.code == 404:
        return set(), -1

    # Make the BeautifulSoup object using the lxml parser.
    soup = bs4.BeautifulSoup(data, "lxml")

    # Find the list containing the links to artist pages.
    artist_pages = soup.find_all("li", class_="item")
    artist_pages = set(i.a["href"] for i in artist_pages)

    # Find the next page to query, if it exists.
    cur_page_num = soup.find("span", class_=["pager", "chosen", "round4"])
    if cur_page_num:
        cur_page_num = cur_page_num.get_text()
        cur_page_num = int(cur_page_num)
        next_page = f"https://bandcamp.com/artist_index?page={cur_page_num + 1}"
    else:
        next_page = -1

    # Sanity check--the very first page of the artist index
    # doesn't use an <a> tag but a <span> tag.  But since it's
    # just the one page we can hard-code it.
    # if urldata.url.endswith("artist_index"):
    #     next_page = "https://bandcamp.com/artist_index?page=2"

    return artist_pages, next_page

def artist_scraping():
    """
    Go through Bandcamp's Artist Index, collecting the artists on each page,
    and writing them out to file.  Then go to the next page of the Artist
    Index and repeat, until we've hit all pages.
    """
    # Set up the progress bar.
    pbar = tqdm.tqdm(desc="Scraping artist index pages")

    # Set the default start page, which is just the base artist index
    # front page.
    page = "https://bandcamp.com/artist_index"

    # Read the next page to scrape, if the file exists.
    # If it's found, overwrite the default "page" value
    # that was just specified, and update the tqdm progress
    # bar with the number of pages currently scraped.
    if os.path.isfile(".nextpage"):
        page = open(".nextpage", "r", encoding="utf8").read().strip()
        # Pull the page number and update TQDM with that number.
        pagenum = page.split("=")[-1]
        pbar.update(int(pagenum))

    # Run the first query to get us started...
    artists, page = get_artist_pages(page)
    if page == -1: return

    # ...and then we're of to the races with the main loop.
    # We'll break the loop when we get `page=-1` returned
    # from the get_artist_pages() function, which should
    # mean we hit the final page and there's nothing more
    # we can do.
    while page != -1:
        # Write the artists list out to file.
        _write_out(artists, "Artist Pages.txt")

        # Write out the next URL to scrape, so we can resume later
        # if something goes wrong.
        with open(".nextpage", "w", encoding="UTF8") as F:
            F.write(page)

        # Courtesy sleep, 5s, to not spam the Bandcamp servers.
        # That would be bad ettiquitte.
        sleep(1)

        # Get the artist links from the next page in the list.
        artists, page = get_artist_pages(page)

        # Update the progress bar
        pbar.update(1)


def get_song_data(url):
    """
    Retrieve the page for an album, and for each song, pull:
    -- Artist
    -- Song Title
    -- Album title
    -- Song lyrics, if present
    -- Song tags
    -- Year of release
    -- URL of the album page
    -- Duration of the tracks

    Store the artist name-bio information in a single table.

    Store the artist/song/album/tags in a table.  In place of the
    lyrics, save the name of a .txt file that contains them, and
    a flag for whether the song is instrumental or not.

    Bandcamp uses the schema.org/MusicRecording schema
    for data.  A lot of this is encoded all around the page,
    not in any single place.
    """

    # This page has a stupid number of "A"s in a row,
    # and it breaks BeautifulSoup.
    if url == "https://666alberto666.bandcamp.com/track/pentapl-gico":
        return

    # If `url` is a BeautifulSoup instance--which will
    # happen if get_albums_single_artist returns a
    # page for a single or an album, we're fine.
    # But if it's a string--i.e. a URL--go ahead
    # and get the HTML data and make the BeautifulSoup
    # object.
    if isinstance(url, str):
        # Make the BeautifulSoup object using the lxml parser.
        # Give it a 60 second timeout for extreme cases---this is
        # usually a sign that something hsa gone horribly wrong.
        # Use the func_timeout library to throw an error if this
        # runs that long.
        try:
            url = func_timeout(
                60,
                bs4.BeautifulSoup,
                args=(urlopen(url), "lxml"),
            )
        except FunctionTimedOut:
            return {}

    # The artist name, from the MusicRecording schema.
    artist = url.find("span", itemprop="byArtist").get_text()

    # <meta itemprop="og:type" content="album"> or <... content="song">
    # tells us if the page is an album or a song.
    # is_album = url.find("meta", itemprop="og:type")["content"]

    # <div id="name-section"><h2 class="trackTitle" itemprop="name"> ALBUM NAME <...>
    title = url.find("div", id="name-section")
    title = title.find("h2", class_="trackTitle", itemprop="name").get_text()

    # Release date, in format YYYYMMDD
    released = url.find("meta", itemprop="datePublished")["content"]

    # Lastly, grab the album tags.
    tags = url.find_all("a", class_="tag")
    tags = [i.get_text() for i in tags]

    # Done with all the album data.  Time to get the per-song data.
    # We'll grab the line from the `var TralbumData` object in the embedded
    # JavaScript, which has a `trackinfo` entry.  This is a list of JSONs
    # with track metadata.
    trackdata = re.compile(r"\s+trackinfo: (\[{.*}\])")
    trackdata = json.loads(
        trackdata.findall(url.get_text())[0]
    )
    # Keep only track number and title for each one.
    trackdata = [
        {
            "artist"       : artist.strip(),
            "album title"  : title.strip(),
            "released"     : released.strip(),
            "track title"  : i["title"].strip(),
            "track number" : i["track_num"],
            "length"       : i["duration"],
            "tags"         : tags,
        }
        for i in trackdata
    ]

    # Now, grab the lyrics if there are any.  These are stored in
    # <dd id="_lyrics_1"></dd> tags where "_lyrics_1" means the lyrics
    # of the first track.  Second track would be "_lyrics_2", etc.
    lyrics = url.find_all("dd", id=re.compile("_lyrics_.*"))

    # Convert `lyrics` into a dict of {track_num:lyrics}.
    # This makes it easy to add lyrics to the trackdata list.
    lyrics = {
        int(i["id"].replace("_lyrics_", "")): i.get_text().strip()
        for i in lyrics
    }

    # Now go add any lyrics found into the trackdata list!
    for i in range(len(trackdata)):
        tracknum = trackdata[i]["track number"]
        trackdata[i]["lyrics"] = lyrics.get(tracknum, "")

    return trackdata

def get_albums_single_artist(artist_url):
    """Retrieve the homepage for an artist, and get the links
    to all albums on that page.  Or, if the link redirects
    to a single album/track page (rather than a list of albums/
    tracks), then just scrape that data instead and save it to file.

    Returns a tuple.  The first element is either "error",
    "songs", or "albums", and identifies what the second
    component encodes.  The third element is the `artist_url`.
        -- If "error": the second element is the URL that
           threw the error.  There is no third element.
        -- If "songs": the second element is a dict of
           track data.
        -- If "albums": the second element is a list of
           urls for individual albums/tracks.
    """

    # Some artist links redirect to a particular release.
    # It seems that we can get around this by manually
    # appending "/music" to the end of their address.
    # NOTE: need to test this with bands that have custom
    # domains through BandCamp.  Seems to work using
    # Amon Tobin (music.amontobin.com/music) but needs
    # more stress-testing.
    # First, though, back up the input URL so we can
    # return it later.
    orig_url = artist_url
    url = artist_url.strip().strip("?")
    url = url + "/music"

    # Keep trying to get the URL page until we get it.
    # If there's any error, just skip the page and write
    # the URL out to an error file.
    try:
        data = urlopen(url)
        assert data.code == 200
    except Exception as e:
        print("Error getting artist albums:", e)
        # print("URL:", url)
        # _write_out(url, "ERROR_ARTIST_URLS.log")
        return "error", orig_url

    # Make the BeautifulSoup object using the lxml parser.
    # Give it a 5 second timeout for extreme cases---this is
    # usually a sign that something hsa gone horribly wrong.
    # Use the func_timeout library to throw an error if this
    # runs that long.
    # For some reason, using a longer timeout than 6s consistently
    # causes the func_timeout wrapper to fail, and the call
    # to bs4.BeautifulSoup hangs forever.
    try:
        soup = func_timeout(
            5,
            bs4.BeautifulSoup,
            args=(data, "lxml"),
        )
    except FunctionTimedOut:
        return "error", orig_url

    # Sanity check: check if we're looking at an album
    # page directly, or at a list of available albums.
    # We should have some <li class="music-grid-item">
    # tags if we're looking at a list of items.
    albums = soup.find_all("li", class_="music-grid-item")

    if not albums:
        # The page is just a single album page.  Scrape it
        # while we've got it!  But sometimes there are blank
        # pages, so we need a try/except block to cath
        # any weird errors these cause, and just return
        # the error message.
        try:
            songs = get_song_data(soup)
            return "songs", songs, orig_url
        except:
            return "error", orig_url

    # Return the links to each album on the artist's page.
    # Since the links will just be in the form `/album/whatever`
    # or `track/whatever`, we have to manually prepent `url` to them.
    albums = [url.replace("/music", "") + i.a["href"] for i in albums]
    return "albums", albums, orig_url

def get_albums_all_artists(artist_url_file):
    """
    Iterate over a list of links to artists' home pages.

    :param artist_url_file: string
        Name of the file storing URLS for each artist, one per line.
    """

    # Use this file to store the list of completed artist
    # scrapes.  We'll skip any artist that's already in
    # this set, and save it to file for re-usability.
    if os.path.isfile("finished_artists.txt"):
        finished_artists = open("finished_artists.txt", "r", encoding="utf8").readlines()
        finished_artists = set(i.strip() for i in finished_artists)
    else:
        finished_artists = set()

    # Open up the list of album artists, and filter based on
    # whether or not each item is in the `finished_artist`
    # set.
    artist_urls = open(artist_url_file, "r", encoding="utf8")
    artist_urls = filter(
        lambda x: x.strip() not in finished_artists,
        artist_urls,
    )

    # And now the main loop.
    # DO NOT MULTITHREAD/MULTIPROCESS THIS.  Bandcamp tends to see that
    # as doing bad stuff and will block access with 503 errors.
    artist_urls = list(artist_urls)
    random.shuffle(artist_urls)
    res = map(get_albums_single_artist, artist_urls)

    # Check how many artists, total, have to be scraped.  We'll use this
    # as the total for the TQDM progress bar.
    total = sum(
        1
        for i in open(artist_url_file, "r", encoding="UTF8")
    )

    # Progress bar that we'll update per-page.
    pbar = tqdm.tqdm(
        desc="Scraping pages",
        unit_scale=False,
        total=total,
        smoothing=0.1,
        initial=len(finished_artists),
        ncols=0,
        ascii=True,
    )
    for R in res:
        if R[0] == "error":
            _write_out([R[-1]], "Error URLs.txt")
        elif R[0] == "songs":
            _dump_track_data(R[1])
        elif R[0] == "albums":
            _write_out(R[1], "Album Pages.txt")

            # Scrape album-specific information while we're here.
            for i in R[1]:
                # Sometimes, there are links to wholly different URLs,
                # rather than sub-pages within the current page.
                # If that's the case, there will be multiple "https://"
                # instances in the string.
                if i.count("https://") > 1:
                    # Just take the second part
                    i = "https://" + i.split("https://")[1]
                try:
                    _dump_track_data(get_song_data(i))
                except Exception as e:
                    print("\n", i, e)
                    _write_out([i], "Error URLs.txt")

        # Add the URL to the finished list.
        _write_out([R[-1]], "finished_artists.txt")
        pbar.update(1)

if __name__ == "__main__":
    # Get the artist pages.  This will return without doing anything
    # if we already got all the possible artist pages.
    # get_artist_pages()

    # Go through the list of artists' pages and start scraping albums
    # and songs from them.
    get_albums_all_artists("Artist Pages.txt")
