import numpy as np
from gensim.corpora import Dictionary
from gensim.models.word2vec import Word2Vec
from gensim.utils import simple_preprocess
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences

from nn import text2id, angular_distance

PROMPT = """Input a command, followed by some text to process.  Valid commands:
    - `predict word2vec` to predict the genres of your input using Word2Vec targets.
    - `predict tfidf` to predict the genres of your input using TF-IDF targets.
    - `tag neighbors` to query the most similar tags to your input.
    - `lyric neighbors` to quiery the most similar words from the lyrics to your input.
    - `quit` to exit the program.
    
Example: `predict I wish I had a better example than this`
Your input: """
def process_text_w2v(text, l_w2v, t_w2v, model):
    print(t_w2v.wv.vectors.shape)

    text = simple_preprocess(text)
    text = text2id(text, l_w2v, len(l_w2v.wv.vocab) - 1)
    pad_value = len(l_w2v.wv.vocab) - 1
    text = pad_sequences([text], 300, padding="post", value=pad_value)
    text = l_w2v.wv.vectors[text]
    pred = model.predict(text)
    pred = t_w2v.similar_by_vector(pred[0])

    print("Predicted genres:")
    max_genre_len = max(map(len, (i[0] for i in pred))) + 2
    print(f"{'Genre':<{max_genre_len}}{'Similarity'}")

    for i in pred:
        print(f"{i[0]:<{max_genre_len}}{i[1]}")


def process_text_tfidf(text, l_w2v, d_tags, model):
    text = simple_preprocess(text)
    text = text2id(text, l_w2v, len(l_w2v.wv.vocab) - 1)
    pad_value = len(l_w2v.wv.vocab) - 1
    text = pad_sequences([text], 300, padding="post", value=pad_value)
    text = l_w2v.wv.vectors[text]
    pred = model.predict(text)[0]
    pred = [(d_tags[i], pred[i]) for i in range(pred.shape[0])]
    pred = sorted(pred, key=lambda x: x[1], reverse=True)[:15]
    max_genre_len = max(map(len, (i[0] for i in pred))) + 2
    print(f"{'Genre':<{max_genre_len}}{'Predicted TF-IDF Score'}")
    for i in pred:
        print(f"{i[0]:<{max_genre_len}}{i[1]}")


def check_w2v_neighbors(w2v, text):
    vec = np.sum(
        [
            w2v[i]
            for i in text.split()
            if i in w2v.wv.vocab
        ],
        axis=0,
    )
    predicted_genres = w2v.similar_by_vector(vec, 11)

    print("Your text:", text)
    print("Most similar terms:", ", ".join(i[0] for i in predicted_genres))

if __name__ == "__main__":
    t_w2v = Word2Vec.load("models/word2vec/tags")
    l_w2v = Word2Vec.load("models/word2vec/lyrics")
    model_w2v = load_model("models/baseline dense.model", {"angular_distance":angular_distance})
    model_tfidf = load_model("models/baseline sparse.model")
    d_tags = Dictionary.load("models/tag dictionary.dictionary")

    while True:
        text = input(PROMPT)
        if text.lower().startswith("predict word2vec"):
            # Spit out the genre predictions.
            process_text_w2v(
                text.split(maxsplit=2)[1],
                l_w2v,
                t_w2v,
                model_w2v
            )
        if text.lower().startswith("predict tfidf"):
            process_text_tfidf(
                text.split(maxsplit=2)[1],
                l_w2v,
                d_tags,
                model_tfidf
            )
        elif text.lower().startswith("tag neighbors"):
            check_w2v_neighbors(t_w2v, text.split(maxsplit=2)[-1])
        elif text.lower().startswith("lyric neighbors"):
            check_w2v_neighbors(l_w2v, text.split(maxsplit=2)[-1])
        elif text == "quit":
            break
        else:
            print("ERROR: invalid command.")
