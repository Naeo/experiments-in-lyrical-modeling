from collections import defaultdict
from itertools import compress
import json
from multiprocessing import Pool
import os

from langdetect import detect
from gensim.corpora import Dictionary
from gensim.matutils import corpus2dense
from gensim.models.callbacks import CallbackAny2Vec
from gensim.models.fasttext import FastText
from gensim.models.tfidfmodel import TfidfModel
from gensim.models.ldamulticore import LdaMulticore
from gensim.models.word2vec import Word2Vec
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import strip_punctuation, strip_multiple_whitespaces
import spacy
from tqdm import tqdm as _tqdm


class EpochLogger(CallbackAny2Vec):
    '''Callback to log information about training'''

    def __init__(self, num_docs, total=None, desc="Word2Vec epochs"):
        self.epoch = 0
        self.num_docs = num_docs
        self.total = total
        self.pbar = _tqdm(desc=desc, total=self.total, unit_scale=True, ascii=True)

    def on_epoch_begin(self, model):
        pass

    def on_epoch_end(self, model):
        self.pbar.update(1)

class TqdmCorpus:
    def __init__(self, it):
        self.it = it
        self.len = len(it)
        self.iter_counter = 1

    def __iter__(self):
        yield from tqdm(self.it, desc=f"Pass {self.iter_counter}", total=self.len)
        self.iter_counter += 1

    def __len__(self):
        return self.len

# I only ever use these tqdm options, so re-define them here.
def tqdm(it, **kwargs):
    return _tqdm(it, unit_scale=True, ascii=True, **kwargs)

def word2vec(texts, outfile, size=300, window=5):
    """
    Train a Word2Vec model on the data, then transform it.
    :param texts:
    :return:
    """
    # texts = [i for i in texts if len(i) >= 5]

    # Create the output directory if it doesn't exist
    if not os.path.isdir("models/word2vec"): os.makedirs("models/word2vec")

    OUT = os.path.join("models/word2vec", outfile)
    if os.path.isfile(OUT): return
    logger = EpochLogger(len(texts), 25, f"Word2vec")
    ft = Word2Vec(
        texts,
        sg=0,
        hs=0,
        size=size,
        window=window,
        # Filter out all but the most common tags
        min_count=10,
        iter=25,
        callbacks=[logger],
        workers=3,
    )
    ft.delete_temporary_training_data(replace_word_vectors_with_normalized=True)
    logger.pbar.close()
    ft.callbacks = ()
    ft.save(OUT)

def load_line(line):
    """
    Load a single line from the track data file.
    Return it as a dictionary of {artist, album, tags, lyrics}.
    If the lyrics aren't in English, return an empty dictionary.
    """
    try:
        j = json.loads(line.lower())
        assert isinstance(j, dict)
    except:
        return {}

    out = {
        "artist":    strip_multiple_whitespaces(strip_punctuation(j["artist"])).strip(),
        "album":     j["album title"],
        "tags":      [strip_multiple_whitespaces(strip_punctuation(i)).strip() for i in j["tags"]],
        "lyrics":    simple_preprocess(j["lyrics"]),
        "language": "NA",
    }

    # Check the language of the lyrics, if the text is long enough.
    if len(out["lyrics"]) < 5:
        out["lyrics"] = ""

    return out

def load_data_mp():
    """
    Multiprocessed track loading for extra speed.
    """
    tags = defaultdict(list)
    lyrics = []
    artists = []
    with Pool(3) as P:
        res = P.imap(
            load_line,
            open("Track Data.json", "r", encoding="utf8").readlines(),
            chunksize=50_000,
        )
        for R in tqdm(res, desc="Loading data"):
            # If an empty/error dict was returned
            if R == {}: continue
            if len(R["lyrics"]) >= 20: lyrics.append(R["lyrics"])
            tag_index = f'{R["artist"].lower()} {R["album"].lower()}'
            tags[tag_index] = R["tags"]
            # if len(lyrics) >= 10_000: break

    # Remove artist names from the tags
    artists = set(artists)
    tags = list(tags.values())
    tags = [
        [i for i in j if i not in artists]
        for j in tqdm(tags, desc="Removing artist names from taglist")
    ]

    # Since "sludge metal" = "sludge" + "metal", tokenize every tag
    # so we can look at them decompositionally.
    tags = [
        [W for T in TAGS for W in simple_preprocess(T)]
        for TAGS in tags
    ]

    # Remove named entities--e.g. place names.
    # Try to use the GPU.
    # if spacy.prefer_gpu(): print("Running spaCy NER on GPU")
    # nlp = spacy.load("en_core_web_lg", disable=["pos", "tagger", "parser", "vectors"])
    # gpe_or_loc = set(j.title() for i in tags for j in i)
    # gpe_or_loc = set(
    #     i.lower()
    #     for i in tqdm(gpe_or_loc, desc="Identifying GPE/LOC tags")
    #     if all(W.ent_type_ in ("GPE", "LOC") for W in nlp(i))
    # )
    # print(sorted(gpe_or_loc))
    # print(f"Found {len(gpe_or_loc)} entities to remove.")
    # tags = [
    #     [i for i in j if i not in gpe_or_loc]
    #     for j in tqdm(tags, desc="Removing GPE/LOC tags")
    # ]
    # print(f"Total of {len(set(i for j in tags for i in j))} unique tags remaining.")

    return tags, lyrics

if __name__ == "__main__":
    tags, lyrics = load_data_mp()
    tags = [i for i in tags if len(i) >= 10]
    word2vec(tags, "tags", window=100)
    # lda(tags, "tags")
    del tags

    lyrics = [i for i in tqdm(lyrics) if len(i) >= 10]
    word2vec(lyrics, "lyrics", size=300, window=5)
