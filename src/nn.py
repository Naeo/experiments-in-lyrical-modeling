import json
from functools import partial
from multiprocessing import Pool
import os

from gensim.corpora import Dictionary
from gensim.matutils import corpus2dense
from gensim.models.ldamulticore import LdaMulticore
from gensim.models.tfidfmodel import TfidfModel
from gensim.models.word2vec import Word2Vec
import keras.backend as K
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense, Conv1D, GlobalMaxPooling1D, MaxPooling1D, Activation, Dropout, LSTM, Masking, CuDNNLSTM
from keras.losses import cosine_proximity
from keras.models import Sequential
from keras.preprocessing.sequence import pad_sequences
from keras.utils import plot_model
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm as tqdm_
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import strip_punctuation, strip_multiple_whitespaces
from sklearn.model_selection import train_test_split, ParameterSampler
import tensorflow as tf

def TextEmbedding_worker(inds, text, tags, l_w2v, t_w2v):
    text_yield = l_w2v.wv.vectors[text[inds]]
    tags_yield = np.vstack([transform_data(T, t_w2v) for T in tags[inds]])

    return text_yield, tags_yield

def TextEmbeddingGenerator(text, tags, l_w2v, t_w2v, batch_size=10_000, n_threads=3):
    """Provide a repeatable iterator that takes in text indices and returns
    word vectors for use with the convnet.
    """
    # Lets us more easily index our random selections.
    text = np.array(text)
    tags = np.array([set(i) for i in tags])
    # Only return texts/tags where the tags are nonzero.
    # All zero tags --> no tags were in the word2vec model -->
    # not usable data.
    where_text = np.any(text < np.max(text), axis=1)
    where_tags = np.array([
        any(i in t_w2v.wv.vocab for i in j)
        for j in tags
    ])

    where = where_tags & where_text
    text = text[where]
    tags = tags[where]

    # Infinite loop --> the data never ends --> can do multiple passes
    inds = np.arange(text.shape[0])
    while True:
        np.random.shuffle(inds)
        batch_inds = [inds[i:i+batch_size] for i in range(0, inds.shape[0], batch_size)]

        for inds in batch_inds:
            text_yield = l_w2v.wv.vectors[text[inds]]
            tags_yield = np.vstack([transform_data(T, t_w2v) for T in tags[inds]])
            yield text_yield, tags_yield

        # with Pool(n_threads) as P:
        #     res = P.map_async(
        #         partial(TextEmbedding_worker, text=text, tags=tags, l_w2v=l_w2v, t_w2v=t_w2v),
        #         batch_inds,
        #         chunksize=10,
        #     )
        #
        #     for R in res: yield R

def BagOfWordsLabelGenerator(text, tags, l_w2v, tag_dict, tag_tfidf=None, batch_size=10_000):
    """Provide a repeatable iterator that takes in text indices and returns
    word vectors for use with the convnet.  Tags are returned as tfidf vectors
    instead of dense word2vec vectors.
    """
    # Lets us more easily index our random selections.
    text = np.array(text)
    tags = np.array(tags)

    # Only return texts/tags where the tags are nonzero.
    # All zero tags --> no tags were in the word2vec model -->
    # not usable data.
    where_text = np.any(text < np.max(text), axis=1)
    where_tags = np.array([len(tag_dict.doc2bow(i)) > 0 for i in tags])
    where = where_tags & where_text
    text = text[where]
    tags = tags[where]

    # Infinite loop --> the data never ends --> can do multiple passes
    inds = np.arange(text.shape[0])
    while True:
        np.random.shuffle(inds)
        batch_inds = [inds[i:i+batch_size] for i in range(0, inds.shape[0], batch_size)]
        for i in batch_inds:
            text_yield = l_w2v.wv.vectors[text[i]]
            # Corpus2dense puts documents in columns--transpose to get it in the right shape
            # for Keras.
            if tag_tfidf:
                tags_yield = corpus2dense(
                    [tag_tfidf[tag_dict.doc2bow(j)] for j in tags[i]],
                    len(tag_dict),
                ).T
            else:
                tags_yield = corpus2dense([tag_dict.doc2bow(j) for j in tags[i]], len(tag_dict)).T
                tags_yield = np.minimum(tags_yield, 1)

            yield tuple((text_yield, tags_yield))

def tqdm(it, **kwargs): return tqdm_(it, unit_scale=True, ascii=True, **kwargs, smoothing=0.1,)


def transform_data(text, w2v):
    """
    Given some text (list of list of str) and a path to a word2vec model,
    transform the text using the word2vec model.
    """
    text = np.sum(
        np.array([
            w2v.wv[j]
            if j in w2v.wv
            else np.zeros(w2v.wv.vectors.shape[1])
            for j in text
        ]),
        axis=0,
        keepdims=True,
    )
    if np.any(text):
        text = text / np.linalg.norm(text)

    return text

def text2id(text, w2v, error_val=-1):
    return [
        w2v.wv.vocab[i].index
        if i in w2v.wv.vocab
        else error_val
        for i in text
    ]

def _load_line(s):
    """
    Multithreadable function to process a single line from the data file.
    Returns an empty dict if there are any errors.
    """
    try:    j = json.loads(s.lower())
    except: return {}
    if not isinstance(j, dict): return {}
    curtrack = {
        "artist": j["artist"],
        "lyrics": simple_preprocess(j["lyrics"]),
        "tags":   [strip_multiple_whitespaces(strip_punctuation(i)).strip() for i in j["tags"]],
    }

    curtrack["tags"] = [W for T in curtrack["tags"] for W in simple_preprocess(T)]

    if len(curtrack["lyrics"]) >= 20:
        return curtrack
    else:
        return {}

def load_data_mp(n_threads=3, debug_nrows=None):
    tracks = []

    with Pool(n_threads) as P:
        res = P.imap_unordered(
            _load_line,
            tqdm(open("Track Data.json", "r", encoding="utf8"), desc="Loading data"),
            chunksize=100_000,
        )
        for i in res:
            if i: tracks.append(i)
            if debug_nrows is not None and len(tracks) >= debug_nrows: break

    return tracks

def angular_distance(y_true, y_pred):
    return tf.math.acos(-cosine_proximity(y_true, y_pred)) * (180 / np.pi)

def make_model(
        data,
        activation="relu",
        kernel_size=3,
        kernel_stride=2,
        pool_size=3,
        pool_stride=2,
        n_filters=256,
        n_conv_layers=3,
        max_input_len=300,
        batch_size=32,
        epochs=10,
        loss="cosine_proximity",
        use_lstm=False,
        use_dense_targets=False,
        use_tfidf=True,
        optimizer="rmsprop",
        outfile="models/lyrics predictor.model",
        save=True,
):
    """
    Build a model using the scikit-learn wrapper, for grid searching.
    """
    # Prepare the data
    t_w2v = Word2Vec.load(f"models/word2vec/tags")
    l_w2v = Word2Vec.load(f"models/word2vec/lyrics")

    t_w2v.delete_temporary_training_data(replace_word_vectors_with_normalized=True)
    l_w2v.delete_temporary_training_data(replace_word_vectors_with_normalized=True)

    lyrics = [text2id(i["lyrics"], l_w2v, len(l_w2v.wv.vocab)) for i in data]
    tags = [i["tags"] for i in data]
    pad_value = len(l_w2v.wv.vocab)

    # LSTMs can support variable-length inputs, so we only need to pad if the first
    # layer is convolutional.
    lyrics = pad_sequences(lyrics, max_input_len, padding="post", value=pad_value)
    l_w2v.wv[pad_value] = np.zeros(l_w2v.wv.vectors.shape[1])
    x_train, x_test, y_train, y_test = train_test_split(
        lyrics,
        tags,
        train_size=0.8,
        test_size=0.2,
    )

    """Build the model"""
    model = Sequential()
    if use_lstm == True:
        model.add(LSTM(128, return_sequences=True, input_shape=(None, l_w2v.wv.vectors.shape[1])))
        model.add(Activation(activation))
    else:
        model.add(Conv1D(
            filters=n_filters,
            kernel_size=kernel_size,
            # strides=kernel_stride,
            input_shape=(max_input_len, l_w2v.wv.vectors.shape[1]),
        ))
        model.add(Activation(activation))
        model.add(MaxPooling1D(pool_size=pool_size))
    for _ in range(n_conv_layers - 1):
        model.add(Conv1D(
            filters=n_filters,
            kernel_size=kernel_size,
            # strides=kernel_stride,
        ))
        model.add(Activation(activation))
        model.add(MaxPooling1D(pool_size=pool_size))
    model.add(GlobalMaxPooling1D())
    model.add(Dropout(0.25))

    # How we construct the size of the dense layer depends on whether we're using
    # dense/word2vec vectors, or sparse/bag-of-words vectors as prediction targets.
    if use_dense_targets == True:
        train_data      = TextEmbeddingGenerator(x_train, y_train, l_w2v, t_w2v, batch_size)
        validation_data = TextEmbeddingGenerator(x_test,  y_test,  l_w2v, t_w2v, batch_size)
        model.add(Dense(t_w2v.wv.vectors.shape[1]))
        model.compile(
            optimizer=optimizer,
            loss=loss,
            metrics=["cosine", angular_distance],
        )
    else:
        tag_dict = Dictionary(tqdm(tags, desc="Training Dictionary() on tags"))
        tag_dict.filter_extremes(no_above=.1, no_below=100)
        if use_tfidf == True:
            tag_tfidf       = TfidfModel(tag_dict.doc2bow(i) for i in tqdm(tags, desc="Fitting TF-IDF model to tags"))
            tag_tfidf.save("models/tag tfidf.tfidf")
        else:
            tag_tfidf = False
        train_data      = BagOfWordsLabelGenerator(x_train, y_train, l_w2v, tag_dict, tag_tfidf, batch_size)
        validation_data = BagOfWordsLabelGenerator(x_test,  y_test,  l_w2v, tag_dict, tag_tfidf, batch_size)
        model.add(Dense(len(tag_dict)))
        model.compile(
            optimizer=optimizer,
            loss=loss,
            metrics=["mean_squared_logarithmic_error"],
        )
        tag_dict.save("models/tag dictionary.dictionary")

    # plot_model(model, show_shapes=True, to_file=f"{outfile}.png")
    print(model.summary())

    if save == True:
        epoch_saver = ModelCheckpoint(f"{outfile} "+"epoch {epoch} loss={loss:.4f}.model", monitor="loss", save_best_only=True)
        callbacks = [epoch_saver]
    else:
        callbacks = []

    model.fit_generator(
        train_data,
        epochs=epochs,
        steps_per_epoch=(len(x_train) // batch_size) + 1,
        verbose=1,
        callbacks=callbacks,
        validation_data=validation_data,
        validation_steps=(len(x_test) // batch_size) + 1,
        # the generator isn't actually thread-safe; workers=0 will
        # run it in the main thread, which is good enough, since it
        # manipulates data only from memory anyways.
        workers=0,
    )
    history = model.history.history
    model.save(filepath=f"{outfile}.model")

    # End the GPU/TensorFlow session and clear all memory.
    K.clear_session()

    # Collect training results to be returned
    ret = dict(
        activation       = activation,
        kernel_size      = kernel_size,
        kernel_stride    = kernel_stride,
        pool_size        = pool_size,
        pool_stride      = pool_stride,
        n_filters        = n_filters,
        n_conv_layers    = n_conv_layers,
        max_input_length = max_input_len,
        batch_size       = batch_size,
        use_lstm         = use_lstm,
        training_history = history,
    )
    ret = json.dumps(ret)

    return ret

def make_model_testing(
        data,
        activation="relu",
        kernel_size=3,
        kernel_stride=2,
        pool_size=3,
        pool_stride=2,
        n_filters=256,
        n_conv_layers=3,
        max_input_len=300,
        batch_size=32,
        epochs=10,
        loss="cosine_proximity",
        use_lstm=False,
        use_dense_targets=False,
        use_tfidf=True,
        optimizer="rmsprop",
        outfile="models/lyrics predictor.model",
        save=True,
):
    """
    Build a model using the scikit-learn wrapper, for grid searching.
    """
    # Prepare the data
    t_w2v = Word2Vec.load(f"models/word2vec/tags")
    l_w2v = Word2Vec.load(f"models/word2vec/lyrics")

    t_w2v.delete_temporary_training_data(replace_word_vectors_with_normalized=True)
    l_w2v.delete_temporary_training_data(replace_word_vectors_with_normalized=True)

    lyrics = [text2id(i["lyrics"], l_w2v, len(l_w2v.wv.vocab)) for i in data]
    tags = [i["tags"] for i in data]
    pad_value = len(l_w2v.wv.vocab)

    # LSTMs can support variable-length inputs, so we only need to pad if the first
    # layer is convolutional.
    lyrics = pad_sequences(lyrics, max_input_len, padding="post", value=pad_value)
    l_w2v.wv[pad_value] = np.zeros(l_w2v.wv.vectors.shape[1])
    x_train, x_test, y_train, y_test = train_test_split(
        lyrics,
        tags,
        train_size=0.8,
        test_size=0.2,
    )

    """Build the model"""
    model = Sequential()
    # model.add(Input())
    # Masking for the LSTM layer
    model.add(Masking(input_shape=(None, l_w2v.wv.vectors.shape[1])))
    model.add(LSTM(128, return_sequences=True))
    model.add(Dropout(0.25))
    model.add(LSTM(64, return_sequences=True))
    model.add(Dropout(0.25))
    model.add(LSTM(64))
    model.add(Dropout(0.25))

    # How we construct the size of the dense layer depends on whether we're using
    # dense/word2vec vectors, or sparse/bag-of-words vectors as prediction targets.
    if use_dense_targets == True:
        train_data      = TextEmbeddingGenerator(x_train, y_train, l_w2v, t_w2v, batch_size)
        validation_data = TextEmbeddingGenerator(x_test,  y_test,  l_w2v, t_w2v, batch_size)
        model.add(Dense(t_w2v.wv.vectors.shape[1]))
        model.compile(
            optimizer=optimizer,
            loss=loss,
            metrics=[angular_distance],
        )
    else:
        tag_dict = Dictionary(tqdm(tags, desc="Training Dictionary() on tags"))
        tag_dict.filter_extremes(no_above=.1, no_below=100)
        train_data      = BagOfWordsLabelGenerator(x_train, y_train, l_w2v, tag_dict, False, batch_size)
        validation_data = BagOfWordsLabelGenerator(x_test,  y_test,  l_w2v, tag_dict, False, batch_size)
        model.add(Dense(len(tag_dict)))
        model.compile(
            optimizer=optimizer,
            loss=loss,
            # metrics=["mean_squared_logarithmic_error"],
        )
        tag_dict.save("models/tag dictionary.dictionary")

    # plot_model(model, show_shapes=True, to_file=f"{outfile}.png")
    print(model.summary())

    if save == True:
        epoch_saver = ModelCheckpoint(f"{outfile}.model", monitor="loss", save_best_only=True)
        callbacks = [epoch_saver]
    else:
        callbacks = []

    model.fit_generator(
        train_data,
        epochs=epochs,
        steps_per_epoch=(len(x_train) // batch_size) + 1,
        verbose=1,
        callbacks=callbacks,
        validation_data=validation_data,
        validation_steps=(len(x_test) // batch_size) + 1,
        # the generator isn't actually thread-safe; workers=0 will
        # run it in the main thread, which is good enough, since it
        # manipulates data only from memory anyways.
        workers=0,
    )
    history = model.history.history
    model.save(filepath=f"{outfile}.model")

    # End the GPU/TensorFlow session and clear all memory.
    K.clear_session()

    # Collect training results to be returned
    ret = dict(
        activation       = activation,
        kernel_size      = kernel_size,
        kernel_stride    = kernel_stride,
        pool_size        = pool_size,
        pool_stride      = pool_stride,
        n_filters        = n_filters,
        n_conv_layers    = n_conv_layers,
        max_input_length = max_input_len,
        batch_size       = batch_size,
        use_lstm         = use_lstm,
        training_history = history,
    )
    ret = json.dumps(ret)

    return ret

if __name__ == "__main__":
    data = load_data_mp(3)

    res = make_model_testing(
        data,
        epochs=2,
        batch_size=256,
        outfile="models/baseline dense lstm",
        use_dense_targets=True,
        loss="cosine_proximity",
        activation="relu",
    )
    with open("models/model stats.json", "a", encoding="utf8") as F:
        F.write("BASELINE, DENSE, MASKED LSTM: " + res + "\n")
    res = make_model_testing(
        data,
        epochs=2,
        batch_size=256,
        outfile="models/baseline sparse tfidf lstm",
        use_dense_targets=False,
        use_tfidf=True,
        loss="mean_squared_error",
        activation="relu",
    )
    with open("models/model stats.json", "a", encoding="utf8") as F:
        F.write("BASELINE, SPARSE, NON-TFIDF, MASKED LSTM: " + res + "\n")


    res = make_model(
        data,
        epochs=2,
        batch_size=256,
        outfile="models/baseline dense convnet",
        use_dense_targets=True,
        loss="cosine_proximity",
        activation="relu",
    )
    with open("models/model stats.json", "a", encoding="utf8") as F:
        F.write("BASELINE, DENSE, CONVNET: " + res + "\n")
    res = make_model(
        data,
        epochs=2,
        batch_size=256,
        outfile="models/baseline sparse tfidf convnet",
        use_dense_targets=False,
        use_tfidf=True,
        loss="mean_squared_error",
        activation="relu",
    )
    with open("models/model stats.json", "a", encoding="utf8") as F:
        F.write("BASELINE, SPARSE, NON-TFIDF, MASKED LSTM: " + res + "\n")

    # params = ParameterSampler(
    #     {
    #         "activation": ["relu", "linear", "softmax", "tanh"],
    #         "kernel_size": [((int(i),)) for i in np.arange(2,10).astype(int)],
    #         "kernel_stride": [((int(i),)) for i in np.arange(2,10).astype(int)],
    #         "pool_size":[((int(i),)) for i in np.arange(2,10).astype(int)],
    #         "pool_stride":[((int(i),)) for i in np.arange(2,10).astype(int)],
    #         "n_conv_layers": [2,3,5,7,9],
    #         "max_input_len":[50,100,300],
    #         "use_lstm":[True, False],
    #     },
    #     1000,
    # )
    # for P in params:
    #     res = ""
    #     try:
    #         res = make_model(data, epochs=2, batch_size=256, **P)
    #     except Exception as e:
    #         print(e)
    #         continue
    #     if res:
    #         with open("models/model stats.json", "a", encoding="utf8") as F:
    #             F.write(res + "\n")